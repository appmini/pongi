const {Blog, Player} = require('../model/userEliFr');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const manageLogin = async (req, res) => {
	// check si le le username et le pw sont fournis
	if (!username || !password) {
		return res.status(400).json({
			message: 'Vos pseudos et mot de passe sont obligatoires.',
		});
	}

	const foundUser = await Player.findOne({username: username}).exec();
	if (!foundUser) {
		return res.status(401).json({message: "Vous n'êtes pas autorisé "}); //Unauthorized
	}
	// Match du mot de passe
	const match = await bcrypt.compare(pwd, foundUser.password);
	// const match = bcrypt.compare(pwd, foundUser.password);
	//## si MATCH alors on créé les TOKEN
	if (match) {
		// ici creation du  JWT (normal et refresh)
		const accessToken = jwt.sign(
			{username: foundUser.username},
			process.env.ACCESS_TOKEN_SECRET,
			{expiresIn: '90s'}
		);
		const refreshToken = jwt.sign(
			{
				username: foundUser.username,
			},
			process.env.REFRESH_TOKEN_SECRET,
			{
				expiresIn: '1d',
			}
		);
		// Sauvegarde du refreshTOKEN
		foundUser.refreshToken = refreshToken;
		const result = await foundUser.save();
		console.log('🚀 ~ manageLogin ~ result:', result);

		res.cookie('refreshToken', refreshToken, {
			httpOnly: true,
			secure: true,
			sameSite: 'none',
		});
		res.json({accessToken});
	} else {
		return res.status(401).json({message: "Vous n'êtes pas autorisé "}); //Unauthorized
	}
};

module.exports = {manageLogin};
