const {Player} = require('../model/userEliFr');
const bcrypt = require('bcrypt');
//

const manageNewUser = async (req, res) => {
	const {username, password} = req.body;
	// check si le le username et le pw sont fournis
	if (!username || !password)
		return res.status(400).json({
			message: 'Le pseudo et le mot de pass sont requis ou ne sont pas conformes',
		});

	//
	// recherche d'un doublon 'username' dans la DB
	const copy = await Player.findOne({username: username}).exec();

	if (copy) {
		return res
			.status(409)
			.send({message: 'Utilisateur existant ! (conflit)'}); //Conflict}
	}

	try {
		//Password encryptage
		const hashedPW = await bcrypt.hash(password, 10);

		// creer et enregistrer le nouvel utilisateur /player
		const newPlayer = await Player.create({
			username: username,
			password: hashedPW,
		});
		// res.status(201).json({succes: `Nouveau joueur ${newPlayer} créé !`});
		res.status(201).render('../html/ping.html', {user: user});
	} catch (err) {
		// res.status(500).json({message: err.message, err: err});
		res.status(500).render('error/404', {user: user});
	}
};
module.exports = {manageNewUser};
