const {Blog, Player} = require('../model/userEliFr');

const manageLogout = async (req, res) => {
	// On client, also delete the accessToken

	const cookies = req.cookies;
	console.log('🚀 ~ manageLogout COOKIES : ', cookies);
	if (!cookies?.jwt)
		return res
			.status(204)
			.json({'message du LOGOUT COntrol': 'PAS DE CONTENU'}); // SUCCES MAIS PAS DE CONTENU; //No content
	const refreshToken = cookies.jwt;
	console.log('🚀 ~ manageLogout ~ refreshToken:', refreshToken);

	// Is refreshToken in db?
	const foundUser = await Player.findOne({refreshToken}).exec();

	if (!foundUser) {
		res.clearCookie('jwt', {
			httpOnly: true,
			secure: true, // à modifier en PROD
			sameSite: 'none',
		});
		return res
			.status(204)
			.json({'message du LOGOUT COntrol': 'PAS DE CONTENU'}); // SUCCES MAIS PAS DE CONTENU
	}

	// Delete du refreshTOKEN dans la DB
	foundUser.refreshToken = '';
	const result = await foundUser.save();

	res.clearCookie('jwt', {
		httpOnly: true,
		secure: true, // à modifier en PROD
		sameSite: 'none',
	});
	res.sendStatus(204); // PAS D CONTENU
};

module.exports = {manageLogout};
