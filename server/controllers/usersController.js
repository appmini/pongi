const Player = require('../model/userEliFr');

const getAllUsers = async (req, res) => {
	const players = await Player.find();
	if (!players) return res.status(204).json({message: 'No users found'});
	res.json(players);
};

const deleteUser = async (req, res) => {
	if (!req?.body?.id)
		return res.status(400).json({message: 'User ID required'});
	const user = await Player.findOne({_id: req.body.id}).exec();
	if (!user) {
		return res.status(204).json({
			message: `Ce pseudo de joueur  ${req.body.id}n'existe pas`,
		});
	}
	const result = await user.deleteOne({_id: req.body.id});
	res.json(result);
};

const getUser = async (req, res) => {
	if (!req?.params?.id)
		return res.status(400).json({message: 'User ID required'});
	const user = await Player.findOne({_id: req.params.id}).exec();
	if (!user) {
		return res
			.status(204)
			.json({message: `User ID ${req.params.id} not found`});
	}
	res.json(user);
};

module.exports = {
	getAllUsers,
	deleteUser,
	getUser,
};
