const {Player} = require('../model/userEliFr');

const jwt = require('jsonwebtoken');
//##
const manageRefreshTOKEN = async (req, res) => {
	const cookies = req.cookies;
	// check si le le username et le pw sont fournis
	if (!cookies?.jwt) {
		return res.status(401).json({
			message: `NON AUTORISE - Souci de COOKIE ou DE JETONS ... à voir .....`,
		});
	}

	const refreshTOKEN = cookies.jwt;

	const foundTOKEN = await Player.findOne({refreshTOKEN}).exec();

	if (!foundTOKEN) {
		return res.status(403).json({message: 'VOTRE SESSION A EXPIRE  '}); //Unauthorized
	}

	jwt.verify(
		refreshTOKEN,
		process.env.REFRESH_TOKEN_SECRET,
		(err, decod) => {
			if (err || foundTOKEN.username !== decod.username) {
				return res
					.status(403) //Token NON VALIDE
					.json({
						message: `VOTRE SESSION A EXPIRE OU VOTRE COMPTE N EXISTE PAS `,
					});
			}
			const accesToken = jwt.sign(
				{username: decod.username},
				process.env.ACCESS_TOKEN_SECRET,
				{expiresIn: '90s'}
			);
			res.json({accessToken: accesToken});
		}
	);
	res.json({
		message: `Bravo ${user} ous êtes connecté !`,
		data: foundUser,
		accessToken,
		refreshTOKEN,
	});
};

module.exports = {manageRefreshTOKEN};
