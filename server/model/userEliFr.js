const mongoose = require('mongoose');
const {Schema} = mongoose;

const blogSchema = new Schema({
	title: String, // String is shorthand for {type: String}
	author: String,
	body: String,
	comments: [{body: String, date: Date}],
	date: {type: Date, default: Date.now},
	hidden: Boolean,
	meta: {
		votes: Number,
		favs: Number,
	},
});

const playerSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true,
		trim: true,
		minlength: 3,
		maxlength: 30,
	},

	password: {
		type: String,
		required: true,
		minlength: 8,
	},
	score: {
		type: Number,
		default: 0,
	},
	createdAt: {
		type: Date,
		default: Date.now,
	},
});

const Player = mongoose.model('Player', playerSchema);
const Blog = mongoose.model('Blog', blogSchema);

module.exports = {Blog, Player};

// email: {
// 	type: String,
// 	required: false,
// 	unique: true,
// 	trim: true,
// 	lowercase: true,
// },

// validate: {
// 	validator: function (v) {
// 		return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
// 			v
// 		);
// 	},
// 	message: 'E-mail non valide, veuiilez corriger votre saisie.',
// },
