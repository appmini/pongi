require('dotenv').config();
const jwt = require('jsonwebtoken');
const ejs = require('ejs');

//
// Création de l'application Express
const express = require('express');
const app = express();
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.set('view engine', 'ejs');

//
// Importation des modules requis
const http = require('http');
const socketio = require('socket.io');

//
const errorHandler = require('./middlewares/errorHandler');
//
const verifyToken = require('./middlewares/verifJWT');
//
const cors = require('cors');
const corsOptions = require('./config/cors-options');
app.use(cors(corsOptions));
//##
const cookieParser = require('cookie-parser');
app.use(cookieParser());

//################# ECRITURE DU JOURNAL DE CONNEXION #############
const {logger} = require('./middlewares/logEvents');
app.use(logger);
//##

const mongoose = require('mongoose');

//################# Servir des fichiers statiques depuis le répertoire "public"###############

const staticRoute = require('./routes/staticFiles');

staticRoute(app);
//################ ROUTES AUTHENTIFICATION ###############################
app.use('/auth', require('./routes/auth'));
app.use('/refresh', require('./routes/refresh'));
app.use('/logout', require('./routes/logout'));
app.use('/reg', require('./routes/register'));

//################## ROUTES DE BASE #########################

app.use('/', require('./routes/root'));

app.use('/log', require('./routes/log'));
app.use('/logout', require('./routes/logout'));

//######### Vérif token pour routes protégées ######################
app.use(verifyToken);

//##################################################################
app.use(errorHandler);

// Définition du répertoire racine pour servir les fichiers statiques
// app.use(express.static(__dirname));

// Création du serveur HTTP
const server = http.createServer(app);

// Création du serveur Socket.IO
const io = socketio(server);

// Écoute des connexions Socket.IO
io.on('connection', (socket) => {
	console.log("Un client s'est connecté");

	// Écoute des messages envoyés par les clients
	socket.on('chat message', (msg) => {
		console.log('message: ' + msg);

		// Envoi du message à tous les clients connectés
		io.emit('chat message', msg);
	});

	// Écoute de la déconnexion d'un client
	socket.on('disconnect', () => {
		console.log("Un client s'est déconnecté");
	});
});
//##################################################################
app.use(errorHandler);

const {connectDB} = require('./config/dbConnect');

connectDB();

const PORT = process.env.PORT || 3000;

mongoose.connection.once('open', () => {
	console.log('📍 Vous etes connecté 🌎');
	app.listen(PORT, () => {
		console.log(' 🎉  SERVER  : http://localhost:' + PORT);
	});
});
