require('dotenv').config();

const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
	const authHeader = req.headers['authorization'];
	if (!authHeader) {
		console.log(
			'🚀 BEARER TOKEN ~ verifyToken ~ authHeader:',
			authHeader
		);
		return res.status(401).json({message: `Aucun jeton d'accès fourni.`});
	}

	const token = authHeader.split(' ')[1];
	console.log(`Voici le TOKEN MiddleWare`, token); // token

	jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoduser) => {
		if (err) {
			return res
				.status(403) //Token NON VALIDE
				.json({message: `Votre jeton d'acces n'est plus valable.`});
		}
		req.user = decoduser.username;
		next();
	});
};

module.exports = verifyToken;
