const {format} = require('date-fns');

const {v4: uuid} = require('uuid');

const fs = require('fs');
const fsPromises = require('fs').promises;
const path = require('path');
//
const date = new Date();
const formattedDate = format(date, 'yyyy-MM-dd');

console.log(formattedDate);
//
const logEvents = async (message, logName) => {
	const dateTime = `${format(new Date(), 'yyyyMMdd\tHH:mm:ss')}`;
	const logItem = `${dateTime}\t${uuid()}\t${message}\n`;

	try {
		if (!fs.existsSync(path.join(__dirname, '..', 'logs'))) {
			await fsPromises.mkdir(path.join(__dirname, '..', 'logs'));
		}

		await fsPromises.appendFile(
			path.join(__dirname, '..', 'logs', logName),
			logItem
		);
	} catch (err) {
		console.log(err);
	}
};

const logger = (req, res, next) => {
	const firstP1 = req.method;
	const firstP2 = req.headers.origin;
	const firstP3 = req.url;
	const firstP = firstP1 + '\t' + firstP2 + '\t' + firstP3;
	logEvents(firstP, 'access.log', 'reqLog.txt');
	//
	defParamReq(req, res);

	console.log(' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');

	console.log('Time:', Date.now());

	next();
};

const defParamReq = (req, res) => {
	const reqpath = req.path;
	const reqtype = req.method;
	const reqbody = req.body;
	const reqquery = req.query;
	const reqheaders = req.headers;
	const reqcookies = req.cookies;
	const reqip = req.ip;
	const reqhost = req.hostname;
	const reqprotocol = req.protocol;
	const reqport = req.port;
	const reqoriginalUrl = req.originalUrl;
	const reqparams = req.params;
	const reqbodyParams = req.bodyParams;
	const reqqueryParams = req.queryParams;
	const reqheadersParams = req.headersParams;
	const reqcookiesParams = req.cookiesParams;
	const reqipParams = req.ipParams;
	const reqhostParams = req.hostParams;
};

module.exports = {logger, logEvents};
