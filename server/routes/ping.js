const express = require('express');
const router = express.Router();
const path = require('path');
const {horoDat} = require('../../public/js/horodate');
//

router.get('/', (req, res) => {
	const dateLisible = horoDat();

	console.log('🚀 ~ ~~~~~~~~~~~~ MIDDLE PING  ~~~~~~~~~~~~~~~~');

	res.render('../../app/htmlBase.html', {dateLisible}); // Ajouter la variable dateLisible aux données rendues
});

module.exports = router;
