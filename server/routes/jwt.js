const express = require('express');

const jwtRoute = express.Router();

//#####################  TEST TOKEN ##############################################
jwtRoute.get('/jwt', (req, res) => {
	console.log('🚀 ~ ~~~~~~~~~~~~ MIDDLE JWT ~~~~~~~~~~~~~~~~');

	const timestamp = Date.now();
	const now = new Date(timestamp + 600000);
	const dateLisible = now.toLocaleString();
	console.log('🚀 ~ JWT Time :', dateLisible);
	//

	//
	const createTokenFromJson = (jsonData, secret, options = {}) => {
		try {
			return jwt.sign(jsonData, secret, options);
		} catch (error) {
			console.log('🚀 ~ createTokenFromJson error:', error);
			return error;
		}
	};

	const jsonData = {
		name: 'Eli',
		email: 'eli@eli.fr',
		password: 'Yayayyyyyaaaaaa',
	};
	const secret = process.env.JWT_SECRET;
	const options = {
		expiresIn: '1h',
		algorithm: 'HS256',
	};

	const token = createTokenFromJson(jsonData, secret, options);

	if (token) {
		console.log('🚀 ~ token:', token);
		res.json(token);
	} else {
		console.log('🚀 ~ token:', token);
		res.json({status: false});
	}
});

module.exports = jwtRoute;
