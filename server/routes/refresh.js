const express = require('express');
const router = express.Router();
const path = require('path');

const refreskTOKENController = require('../controllers/refreshTokenController');

router.get('/', refreskTOKENController.manageRefreshTOKEN);

module.exports = router;
