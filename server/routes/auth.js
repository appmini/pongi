const express = require('express');
const routerAuth = express.Router();
const path = require('path');

const authController = require('../controllers/authController');

routerAuth.post('/', authController.manageLogin);

module.exports = routerAuth;
