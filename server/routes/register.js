const express = require('express');
const router = express.Router();

const registerController = require('../controllers/registerController');
// const registerController = require('../controllers/registerController');

// routerRegister.post('/', registerController.manageNewUser);
router.post('/', registerController.manageNewUser);

module.exports = router;
