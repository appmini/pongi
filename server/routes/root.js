const express = require('express');
const router = express.Router();
const {horoDat} = require('../../public/js/horodate');
//

router.get(['/', '/index'], (req, res) => {
	const dateLisible = horoDat();

	console.log('🚀 ~ ~~~~~~~~~~~~ MIDDLE ACCUEIL SIMPLE /  ~~~~~~~~~~~~~~~~');

	res.render('pages/index', {dateLisible}); // Ajouter la variable dateLisible aux données rendues
});

router.get('/reg', (req, res) => {
	console.log('🚀 ~ ~~~~~~~~~~~~ MIDDLE /contact.html  ~~~~~~~~~~~~~~~~');

	const dateLisible = horoDat();

	res.render('.././views/conlog/register.ejs', {dateLisible});
	//res.render("contact", {titre, root: __dirname + "/views"});
	//
});
//
router.get('/log', (req, res) => {
	console.log('🚀 ~ ~~~~~~~~~~~~ MIDDLE   /resume.html  ~~~~~~~~~~~~~~~~');
	const dateLisible = horoDat();

	res.render('.././views/conlog/log', {dateLisible});
});

router.get('/logout', (req, res) => {
	console.log('🚀 ~ ~~~~~~~~~~~~ MIDDLE   /resume.html  ~~~~~~~~~~~~~~~~');
	const dateLisible = horoDat();

	res.render('.././views/conlog/logout', {dateLisible});
});

//
//
router.get(['/404'], (req, res) => {
	const dateLisible = horoDat();
	console.log('🚀 ~ ~~~~~~~~~~~~ MIDDLE   /404  ~~~~~~~~~~~~~~~~');
	res.render('error/404', {dateLisible});
});

module.exports = router;
