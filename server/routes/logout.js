const express = require('express');
const router = express.Router();
const path = require('path');
const {horoDat} = require('../../public/js/horodate');

const logoutController = require('../controllers/logoutController');

router.get('/', (req, res) => {
	logoutController.manageLogout;
	const dateLisible = horoDat();

	res.render('../../app/logout.html', {dateLisible}); // Ajouter la variable dateLisible aux données rendues
});

module.exports = router;
