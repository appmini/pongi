const express = require('express');
//
const useRoutes = express.Router();
//
//
const {
	getAllUsers,
	createUser,
	updateUser,
	deleteUser,
	getOneUser,
} = require('../../autres/usersControl');
//
useRoutes
	.route('/')
	.get(getAllUsers)
	.post(createUser)
	.put(updateUser)
	.delete(deleteUser);

useRoutes.route('/:id').get(getOneUser);

module.exports = useRoutes;
