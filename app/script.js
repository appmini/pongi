const canvas = document.getElementById('pingPong');
const ctx = canvas.getContext('2d');

const paddleWidth = 10;
const paddleHeight = 100;
const paddleSpeed = 5;
const ballSize = 10;
const ballSpeed = 3;

let player1Score = 0;
let player2Score = 0;

let paddle1Y = canvas.height / 2 - paddleHeight / 2;
let paddle2Y = canvas.height / 2 - paddleHeight / 2;

let ballX = canvas.width / 2;
let ballY = canvas.height / 2;
let ballDX = ballSpeed;
let ballDY = ballSpeed;

function drawPaddle(x, y) {
	ctx.fillStyle = '#fff';
	ctx.fillRect(x, y, paddleWidth, paddleHeight);
}

function drawBall(x, y) {
	ctx.fillStyle = '#fff';
	ctx.fillRect(x, y, ballSize, ballSize);
}

function drawScore() {
	ctx.font = '24px Arial';
	ctx.fillStyle = '#fff';
	ctx.fillText(`Player 1: ${player1Score}`, 10, 30);
	ctx.fillText(`Player 2: ${player2Score}`, canvas.width - 100, 30);
}

function update() {
	ballX += ballDX;
	ballY += ballDY;

	if (ballY + ballSize > canvas.height) {
		ballDY = -ballDY;
	} else if (ballY < 0) {
		ballDY = -ballDY;
	}

	if (ballX + ballSize > canvas.width) {
		player1Score++;
		resetBall();
	} else if (ballX < 0) {
		player2Score++;
		resetBall();
	}

	if (
		ballX + ballSize > canvas.width - paddleWidth &&
		ballY + ballSize > paddle2Y &&
		ballY < paddle2Y + paddleHeight
	) {
		ballDX = -ballDX;
	}

	if (
		ballX < paddleWidth &&
		ballY + ballSize > paddle1Y &&
		ballY < paddle1Y + paddleHeight
	) {
		ballDX = -ballDX;
	}

	if (keyDown['ArrowUp']) {
		paddle1Y -= paddleSpeed;
	} else if (keyDown['ArrowDown']) {
		paddle1Y += paddleSpeed;
	}

	if (keyDown['w']) {
		paddle2Y -= paddleSpeed;
	} else if (keyDown['s']) {
		paddle2Y += paddleSpeed;
	}

	if (paddle1Y < 0) {
		paddle1Y = 0;
	} else if (paddle1Y + paddleHeight > canvas.height) {
		paddle1Y = canvas.height - paddleHeight;
	}

	if (paddle2Y < 0) {
		paddle2Y = 0;
	} else if (paddle2Y + paddleHeight > canvas.height) {
		paddle2Y = canvas.height - paddleHeight;
	}
}

function resetBall() {
	ballX = canvas.width / 2;
	ballY = canvas.height / 2;
	ballDX = -ballDX;
}

function draw() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	drawPaddle(0, paddle1Y);
	drawPaddle(canvas.width - paddleWidth, paddle2Y);
	drawBall(ballX, ballY);
	drawScore();
}

let keyDown = {};

window.addEventListener('keydown', function (e) {
	keyDown[e.key] = true;
});

window.addEventListener('keyup', function (e) {
	delete keyDown[e.key];
});

function gameLoop() {
	update();
	draw();
	requestAnimationFrame(gameLoop);
}

gameLoop();
